package com.epam;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public class SampleTestNgIosTest {


    protected static String baseUrl;
    protected static Capabilities capabilities;
    protected IOSDriver driver;
    protected Dimension screenSize;

    @BeforeSuite
    public void initTestSuite() {
        SuiteConfiguration config = new SuiteConfiguration();
        //capabilities = config.getIosWebCapabilities();
        capabilities = config.getIosCalendarCapabilities();
        baseUrl = "google.com";
    }

    @BeforeMethod
    public void initWebDriver() {
        try {
            driver = new IOSDriver(new URL("http://localhost:4444/wd/hub/"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCalendar() {
        pause(5000);
        WebElement from = driver.findElementByIosNsPredicate("type=='XCUIElementTypeButton' AND name CONTAINS 'Friday'");
        WebElement to = driver.findElementByIosNsPredicate("type=='XCUIElementTypeButton' AND name CONTAINS 'Monday'");

        swipeByElements(from, to);
        pause(5000);
        swipeByCoords(200, 350, 200, 150);
        pause(5000);
    }

    @Test
    public void testSafari() {
        RequestFilter requestFilter = new RequestFilter() {
            @Override
            public HttpResponse filterRequest(HttpRequest httpRequest, HttpMessageContents httpMessageContents, HttpMessageInfo httpMessageInfo) {
                if(httpRequest.getUri().contains("appium.io")){
                    httpRequest.setUri(httpRequest.getUri().replace("appium.io", "epam.com"));
                }

                return null;
            }
        };

        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.start(8889);

        proxy.addRequestFilter(requestFilter);
        driver.get("http://appium.io");
        Assert.assertEquals(driver.getCurrentUrl(), "http://appium.io/", "Some error message");


        verticalSwipeByPercentages(0.8, 0.2, 0.5);
        pause(5000);
        swipeByCoords(130, 200, 30, 200);
        pause(5000);
    }

    private void pause(int i) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Vertical Swipe by percentages
    private void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {
        // from this example we can see how it can be complicated to get coordinates.
        // for this site we can see that navigation bar have the same width as device,
        // and visible height if the site is about 10 * height of nav bar
        Dimension size = driver.findElement(By.tagName("nav")).getSize();

        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage * 10);
        int endPoint = (int) (size.height * endPercentage * 10);

        new TouchAction(driver)
                .press(point(anchor, startPoint))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(anchor, endPoint))
                .release().perform();
    }

    private void swipeByElements(WebElement from, WebElement to) {

        Rectangle fromRectangle = from.getRect();
        Rectangle toRectangle = to.getRect();

        Point fromPoint = new Point(fromRectangle.x + fromRectangle.width / 2, fromRectangle.y + fromRectangle.height / 2);
        Point toPoint = new Point(toRectangle.x + toRectangle.width / 2, toRectangle.y + toRectangle.height / 2);


        new TouchAction(driver)
                .press(point(fromPoint))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(toPoint))
                .release().perform();
    }

    //Vertical Swipe by coords
    private void swipeByCoords(int startX, int startY, int endX, int endY) {

        new TouchAction(driver)
                .press(point(new Point(startX, startY)))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(new Point(endX, endY)))
                .release().perform();
    }
}

