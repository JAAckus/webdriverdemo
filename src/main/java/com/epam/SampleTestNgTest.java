package com.epam;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class SampleTestNgTest {


    protected static String baseUrl;
    protected static Capabilities capabilities;
    protected AppiumDriver driver;

    @BeforeSuite
    public void initTestSuite() {
        SuiteConfiguration config = new SuiteConfiguration();
//        capabilities = config.getAndroidCapabilities();
        capabilities = config.getAndroidCalculatorCapabilities();
        baseUrl = "google.com";
    }

    @BeforeMethod
    public void initWebDriver() {
        try {
            driver = new AndroidDriver(new URL("http://localhost:4444/wd/hub/"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCalc() {
        driver.findElementById("digit_5").click();
        driver.findElementByAccessibilityId("plus").click();
        driver.findElementByXPath("//*[@text='7']").click();
        driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiSelector().className(\"android.view.ViewGroup\")" +
                        ".childSelector(new UiSelector().className(\"android.widget.Button\").text(\"=\"))")).click();
        List<MobileElement> textView = driver.findElementsByClassName("android.widget.TextView");
        Assert.assertEquals(Integer.parseInt(textView.get(1).getText()), 5 + 7,
                "Sum doesn't match");

        // Swipe right to left to open extra operations
        Dimension screenSize = driver.manage().window().getSize();
        swipeByCoords(screenSize.getWidth() - 10, screenSize.getHeight() - 150,
                50, screenSize.getHeight() - 150);

        Assert.assertTrue(driver.findElementByAccessibilityId("cosine").isDisplayed(),
                "Cosine button is not visible");

        driver.getOrientation();
//        driver.rotate(ScreenOrientation.LANDSCAPE);
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
    }

    @Test
    public void testChrome() {
        driver.get("http://appium.io");
        Assert.assertEquals(driver.getCurrentUrl(),"http://appium.io/", "Some error message");

//        swipeByCoords(500, 1500, 500, 500);

        verticalSwipeByPercentages(.90, .10, .50);
    }

    //Vertical Swipe by percentages
    private void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {

        driver.context("NATIVE_APP");
        MobileElement layout = (MobileElement) driver.findElementsByClassName("android.widget.FrameLayout").get(0);
        Dimension size = layout.getSize();

        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * endPercentage);

        new TouchAction(driver)
                .press(point(anchor, startPoint))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(anchor, endPoint))
                .release().perform();

        driver.context("CHROMIUM");
    }

    //Vertical Swipe by coords
    private void swipeByCoords(int startX, int startY, int endX, int endY) {

        new TouchAction(driver)
                .press(point(new Point(startX, startY)))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(new Point(endX, endY)))
                .release().perform();
    }
}
