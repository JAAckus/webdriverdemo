package com.epam;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SuiteConfiguration {

    public SuiteConfiguration() {
    }


    public Capabilities getAndroidCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("avd", "Pixel_2_API_28");

        return capabilities;
    }

    public Capabilities getIosWebCapabilities() {
        //ios_webkit_debug_proxy -c 6C108172-3CC7-4B97-9143-78CC98B9E20A:27753 - for real devices
        //appium -p 4445
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "12.0");
        capabilities.setCapability("deviceName", "IPAD");
        capabilities.setCapability("udid", "6C108172-3CC7-4B97-9143-78CC98B9E20A");
        capabilities.setCapability("browserName", "Safari");

        return capabilities;
    }

    public Capabilities getIosCalendarCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "12.0");
        capabilities.setCapability("deviceName", "IPAD");
        capabilities.setCapability("udid", "6C108172-3CC7-4B97-9143-78CC98B9E20A");
        capabilities.setCapability("bundleId", "com.apple.mobilecal");

        return capabilities;
    }


    public Capabilities getAndroidCalculatorCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android emulator");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("avd", "Nexus_7_API_28");
//        capabilities.setCapability("orientation", "LANDSCAPE"); // Value should be in capital case
        return capabilities;
    }
}
